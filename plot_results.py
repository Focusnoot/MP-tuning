import matplotlib.pyplot as plt
import matplotlib.patches as patches
import numpy as np
from matplotlib.ticker import FuncFormatter
from sys import argv as commandline_params
from json import load
from math import log
# from scipy.interpolate import make_interp_spline, BSpline


FLOAT = "float"
INT = "int"
STRING = "str"


def default_or_config(default, config, config_key):
    if (config_key in config):
        return config[config_key]

    return default


def sort_data(data, config):
    str_to_type = {
        "int": int,
        "float": float
    }
    sort_type = default_or_config(str, config, "type")
    if sort_type in str_to_type:
        sort_type = str_to_type[sort_type]

    data.sort(key=lambda x:sort_type(x[int(default_or_config(0, config,
        "key"))]), reverse=bool(default_or_config(False, config, "reverse")))


def filter_data_top(data, data_filter_config):
    amount = int(default_or_config(0, data_filter_config, "amount"))
    end = int(int(default_or_config(0, data_filter_config, "end_percent")) /
        100 * len(data))
    start = end - amount

    return data[start:end]


def filter_data_config(data, data_filter_config):
    configs_to_keep = default_or_config([], data_filter_config, "config")

    return [config for config in data if config[:len(configs_to_keep[0])] in
        configs_to_keep]


def filter_data(data, data_filters):
    filter_functions = {
        "top": filter_data_top,
        "config": filter_data_config
    }
    result = []

    for data_filter in data_filters:
        sort_results = default_or_config({}, data_filter, "sort_results")
        if sort_results:
            sort_data(data, sort_results)

        filter_type = default_or_config("", data_filter, "type")

        result = result + filter_functions[filter_type](data, data_filter)

    return result


def gather_data(data, config):
    result = []

    try:
        row_num = int(default_or_config(-1, config, "row"))
        row_type = str(default_or_config(FLOAT, config, "type"))
        row_transform = list(default_or_config([], config, "transform"))
    except (ValueError, TypeError) as e:
        print("An error occured while processing the data options. The option "
              "'row' expects an int, 'type' a string and 'transform' a list.")
        exit()

    # Create a list of ascending integers, with an integer per data row
    if row_num < 0:
        identifier_rows = list(default_or_config([], config, "identifier_rows"))
        cur_identifier = [data[0][id_row] for id_row in identifier_rows]
        counter = 1

        if cur_identifier:
            for row in data:
                temp_identifier = [row[id_row] for id_row in identifier_rows]
                if temp_identifier != cur_identifier:
                    counter += 1
                    cur_identifier = temp_identifier
                result.append(counter)
        else:
            for row in data:
                result.append(counter)
                counter += 1
    else:
        for row in data:
            # Get the actual data on the row_num position
            if row_type == FLOAT:
                new_result = float(row[row_num])
            elif row_type == INT:
                new_result = int(row[row_num])
            elif row_type == STRING:
                new_result = str(row[row_num])
            else:
                new_result = row[row_num]

            # Transform the data. The row_transform variable should contain
            # a list with operations directly followed by a number per
            # operation to be used with it (e.g. ["divide_this", 2]), so the
            # counter is increased twice per operation/loop iteration.
            counter = 0
            while counter < len(row_transform):
                if row_transform[counter] == "divide_this":
                    counter += 1
                    new_result /= row_transform[counter]
                elif row_transform[counter] == "divide_that":
                    counter += 1
                    new_result = row_transform[counter] / new_result
                elif row_transform[counter] == "divide_that_row":
                    counter += 1
                    new_result = int(row[row_transform[counter]]) / new_result
                elif row_transform[counter] == "subtract_this":
                    counter += 1
                    new_result -= row_transform[counter]
                elif row_transform[counter] == "subtract_that":
                    counter += 1
                    new_result = row_transform[counter] - new_result
                elif row_transform[counter] == "multiply":
                    counter += 1
                    new_result *= row_transform[counter]
                elif row_transform[counter] == "multiply_with_row":
                    counter += 1
                    new_result *= int(row[row_transform[counter]])

                counter += 1

            result.append(new_result)

    return result


def gather_ticks(data, config):
    result = []

    start = default_or_config("", config, "start")
    separator = default_or_config("", config, "separator")
    end = default_or_config("", config, "end")
    rows = default_or_config([], config, "rows")
    unique = default_or_config(False, config, "unique")

    for row in data:
        new_tick = start
        for counter, index in enumerate(rows):
            if (counter > 0):
                new_tick += separator
            new_tick += row[index]

        new_tick += end
        if unique and new_tick in result:
            continue

        result.append(new_tick)

    return result


def gather_tick_positions_by_identifier(data, identifiers):
    positions = []

    for identifier in identifiers:
        group_start = -1
        group_end = -1
        i = 0

        while data[i] != identifier:
            i += 1

        group_start = i

        while i < len(data) and data[i] == identifier:
            i += 1

        group_end = i - 1

        # The position is the middle point between the group start and end. Also
        # add 1 to compensate for the fact that the positions start at 1 instead
        # of 0.
        positions.append((group_start + group_end) / 2 + 1)

    return positions


def apply_global_settings(config):
    plt.rcParams['font.size'] = default_or_config(14, config, "font_size")
    # plt.rcParams['font.size'] = default_or_config(6, config, "font_size")


def apply_global_plot_settings(fig, config):
    global_title = default_or_config("", config, "global_title")
    if global_title:
        fig.suptitle(global_title, y=0.99,
            fontsize=default_or_config(18, config, "global_title_font_size"))
            # fontsize=default_or_config(10, config, "global_title_font_size"))

    global_legend_config = default_or_config({}, config, "global_legend")
    if global_legend_config:
        bbox = (default_or_config(None, global_legend_config, "bbox_x"),
            default_or_config(None, global_legend_config, "bbox_y"))
        if bbox[0] == None or bbox[1] == None:
            bbox = None

        handles = fig.axes[0].lines

        if default_or_config("", global_legend_config, "use_hatches"):
            handles = []
            for hatch in default_or_config([], config, "hatches"):
                handles.append(patches.Patch(facecolor="white", hatch=hatch,
                    edgecolor="black"))

        labels = default_or_config([], config, "labels")

        row_title = default_or_config("", global_legend_config, "row_title")
        if row_title:
            labels = [row_title] + labels
            handles = [patches.Patch(facecolor="white")] + handles

        leg = fig.legend(handles=handles,
            loc=default_or_config("best", global_legend_config, "loc"),
            bbox_to_anchor=bbox,
            title=default_or_config("", global_legend_config, "title"),
            ncol=default_or_config(1, global_legend_config, "ncol"),
            labels=labels,
            edgecolor="#000000",
            title_fontsize=default_or_config(16, config, "title_font_size"),
            # title_fontsize=default_or_config(9, config, "title_font_size"),
            markerfirst=default_or_config(True, global_legend_config,
                "markerfirst"))


def apply_plot_settings(ax, data, x_ax_data, config):
    # Use scientific notation (using e^..) if the max y value >= 1000
    if default_or_config("", config, "type") != "roofline":
        ax.ticklabel_format(style="sci", scilimits=(0,4))

    # Add an x range limit to the plot if this option is used
    xlim = default_or_config([], config, "xlim")
    if xlim:
        plt.xlim(xlim[0], xlim[1])

    # Add an y range limit to the plot if this option is used
    ylim = default_or_config([], config, "ylim")
    if ylim:
        plt.ylim(ylim[0], ylim[1])

    # Get the x labels for the plot if this option is used
    xticks = default_or_config({}, config, "xticks")
    if xticks and isinstance(xticks, dict):
        labels = default_or_config([], xticks, "identifier_labels")
        if labels:
            x_positions = gather_tick_positions_by_identifier(x_ax_data, labels)
            labels = default_or_config(labels, xticks, "labels_appearance")
        else:
            labels = default_or_config([], xticks, "labels")
            if labels:
                x_positions = [i for i in range(1, len(labels) + 1)]
            else:
                labels = gather_ticks(data, xticks)
                x_positions = gather_data(data, {"row": -1})

        plt.xticks(x_positions, labels,
            rotation=default_or_config(0, config, "xticks_rotation"))
        ax.tick_params(axis='x', which='minor', length=0)
    elif xticks:
        plt.xticks(xticks)

    # Get the y labels for the plot if this option is used
    yticks = default_or_config([], config, "yticks")
    if yticks:
        plt.yticks(yticks)

    # Turn off top ticks
    if default_or_config(False, config, "ticks_top_off"):
        ax.tick_params(top=False)

    # Add major horizontal lines if this option is used
    if default_or_config(False, config, "horizontal_lines_major"):
        hlines_major_style = default_or_config('-', config,
            "horizontal_lines_major_style")
        ax.grid(which='major', color='black', axis='y',
            linestyle=hlines_major_style)

    # Add major vertical lines if this option is used
    if default_or_config(False, config, "vertical_lines_major"):
        vlines_major_style = default_or_config('-', config,
            "vertical_lines_major_style")
        ax.grid(which='major', color='black', axis='x',
            linestyle=vlines_major_style)

    # Add minor horizontal lines if this option is used
    if default_or_config(False, config, "horizontal_lines_minor"):
        ax.grid(which='minor', color='black', axis='y', linestyle=':')
        ax.minorticks_on()
        ax.tick_params(axis='y', which='minor', length=0)

    # Add a horizontal line if this option is used
    axhline = default_or_config([], config, "axhline")
    if axhline:
        ax.axhline(y=axhline[0], xmin=axhline[1], xmax=axhline[2],
            color=axhline[3], linewidth=axhline[4])

    # Add a grid if this option is used
    if default_or_config(False, config, "grid") and xlim and ylim:
        major_steps = [i for i in range(xlim[0], xlim[1] + 1,
            default_or_config(1, config, "grid_major_step"))]
        minor_steps = [i for i in range(xlim[0], xlim[1] + 1,
            default_or_config(1, config, "grid_minor_step"))]

        ax.set_xticks(major_steps)
        ax.set_xticks(minor_steps, minor=True)
        ax.set_yticks(major_steps)
        ax.set_yticks(minor_steps, minor=True)
        ax.tick_params(which='both', length=0.0)
        ax.xaxis.tick_top()

        ax.grid(which='minor', axis='both', color='black', linestyle='-')

    # Set any major or minor lines (if those options are used) below the plotted
    # data.
    ax.set_axisbelow(True)

    # Set the x, y, and title labels of the plot
    plt.xlabel(default_or_config("", config, "xlabel"))
    plt.ylabel(default_or_config("", config, "ylabel"))
    plt.title(default_or_config("", config, "title"),
        fontsize=default_or_config(16, config, "title_font_size"),
        # fontsize=default_or_config(9, config, "title_font_size"),
        x=default_or_config(0.5, config, "title_x"),
        y=default_or_config(1.0, config, "title_y"))


    # Put the outer x data at the plot edges in case x ticks are used
    if default_or_config(False, config, "tight"):
        plt.autoscale(enable=True, tight=True, axis='x')

    # Manually set the plot margins in case this option is used
    margins = default_or_config([], config, "margins")
    if margins:
        plt.margins(margins[0], margins[1])

    # Change the plot size and position to look good fullscreen on a 23 inch
    # screen
    # plt.subplots_adjust(left=0.1, bottom=0.06, right=0.995, top=0.93,
    plt.subplots_adjust(left=0.05, bottom=0.06, right=0.98, top=0.97,
        wspace=0.2, hspace=0.25)
    # Or to a custom size/position
    custom_subplot_config = default_or_config([], config,
        "custom_subplot_config")
    if custom_subplot_config:
        plt.subplots_adjust(**custom_subplot_config)


    # Add a legend if this option is used
    legend_config = default_or_config({}, config, "legend")
    if legend_config:
        ax.legend(loc=default_or_config("best", legend_config, "loc"),
            numpoints=default_or_config(None, legend_config, "numpoints"),
            title=default_or_config(None, legend_config, "title"))


def gather_data_default(data, config):
    x_ax_data = gather_data(data, default_or_config({}, config, "xdata"))
    y_ax_data = gather_data(data, default_or_config({}, config, "ydata"))

    return (x_ax_data, y_ax_data)


def gather_data_grouped_bar(data, config):
    y_ax_data = gather_data(data, default_or_config({}, config, "ydata"))

    x_data_config = default_or_config({}, config, "xdata")

    # In case we want to identify the groups by more parameters, create a list
    # all_x_ax_data of the format [[...,...], ...]. Else simply [..., ...].
    if isinstance(x_data_config["row"], list):
        all_x_ax_data = [[] for y in y_ax_data]
        cur_x_data_config = {}

        for i in range(0, len(x_data_config["row"])):
            for key in x_data_config:
                cur_x_data_config[key] = x_data_config[key][i]

            x_ax_data = gather_data(data, cur_x_data_config)
            for i in range(0, len(all_x_ax_data)):
                all_x_ax_data[i].append(x_ax_data[i])
    else:
        all_x_ax_data = gather_data(data, x_data_config)

    return (all_x_ax_data, y_ax_data)


def gather_data_stacked_bar(data, config):
    x_ax_data = gather_data(data, default_or_config({}, config, "xdata"))
    all_y_ax_data = []

    all_config_ydata = default_or_config([], config, "ydata")
    for config_ydata in all_config_ydata:
        all_y_ax_data.append(gather_data(data, config_ydata))

    return (x_ax_data, all_y_ax_data)


def gather_data_multiline(data, config):
    all_y_ax_data = []
    all_x_ax_data = []

    x_ax_data = gather_data(data, default_or_config({}, config, "xdata"))
    y_ax_data = gather_data(data, default_or_config({}, config, "ydata"))

    line_data = gather_data(data, default_or_config({}, config, "linedata"))
    label_data = list(set(line_data))
    label_data.sort()

    for label in label_data:
        all_x_ax_data.append([])
        all_y_ax_data.append([])
        for i in range(0, len(line_data)):
            if line_data[i] == label:
                all_x_ax_data[-1].append(x_ax_data[i])
                all_y_ax_data[-1].append(y_ax_data[i])

    # TODO probably make smooth option for this
    # smooth_points = 300
    # for i in range(2, len(all_x_ax_data)):
        # spl = make_interp_spline(all_x_ax_data[i], all_y_ax_data[i], k=3)
        # all_x_ax_data[i] = np.linspace(all_x_ax_data[i][0],all_x_ax_data[i][-1],smooth_points)

        # all_y_ax_data[i] = spl(all_x_ax_data[i])

    return (all_x_ax_data, all_y_ax_data)


def gather_data_crossed_vlines(data, config):
    x_ax_data = gather_data(data, default_or_config({}, config, "xdata"))
    y_ax_data = gather_data(data, default_or_config({}, config, "ydata"))
    label_data = gather_data(data, default_or_config({}, config, "labeldata"))

    all_x_ax_data = []
    all_y_ax_data = []
    unique_labels = default_or_config([], config, "labels")

    for i in range(len(label_data)):
        j = unique_labels.index(str(label_data[i]))
        while j >= len(all_y_ax_data):
            all_y_ax_data.append([])
            all_x_ax_data.append([])

        all_x_ax_data[j].append(x_ax_data[i])
        all_y_ax_data[j].append(y_ax_data[i])

    return (all_x_ax_data, all_y_ax_data)


def gather_data_grid(data, config):
    width_data = gather_data(data, default_or_config({}, config, "widthdata"))
    height_data = gather_data(data, default_or_config({}, config, "heightdata"))
    amount_data = gather_data(data, default_or_config({}, config, "amountdata"))

    dimension_data = []
    for i in range (len(width_data)):
        dimension_data.append((width_data[i], height_data[i]))

    return (dimension_data, amount_data)


def gather_data_violin(data, config):
    x_ax_data, y_ax_data = gather_data_default(data, config)

    violin_ranges = []

    xdata_range = default_or_config({}, config, "xdata_range")

    if xdata_range:
        xdata_min = default_or_config(0, xdata_range, "xdata_min")
        xdata_max = default_or_config(0, xdata_range, "xdata_max")
        xdata_num_ranges = default_or_config(1, xdata_range, "xdata_num_ranges")

        range_size = int((xdata_max - xdata_min) / xdata_num_ranges)

        for i in range(0, xdata_num_ranges):
            violin_ranges.append([])

        for i in range(len(x_ax_data)):
            current_range_max = xdata_min
            current_range_index = 0

            # Add the gflops (y) value of the current shmem usage (x) value to
            # the first range that it has less than the range max shmem usage of
            while current_range_max < xdata_max:
                current_range_max += range_size

                if x_ax_data[i] <= current_range_max:
                    violin_ranges[current_range_index].append(y_ax_data[i])
                    break

                current_range_index += 1
    else:
        sorted_unique_xdata = list(set(x_ax_data))
        sorted_unique_xdata.sort()

        for i in range(0, len(sorted_unique_xdata)):
            violin_ranges.append([])

        for i in range(len(x_ax_data)):
            current_range_index = sorted_unique_xdata.index(x_ax_data[i])
            violin_ranges[current_range_index].append(y_ax_data[i])

    return (x_ax_data, violin_ranges)


def plot_bar(config, x_ax_data, y_ax_data):
    # Matplotlib refuses to plot 0 bar values on the edges of the data, so
    # 'fix' that by making the edges a very low number
    if y_ax_data[0] == 0:
         y_ax_data[0] = 0.0000000000000000000000000001
    if y_ax_data[-1] == 0:
        y_ax_data[-1] = 0.0000000000000000000000000001

    bars = plt.bar(x_ax_data, y_ax_data,
        align=default_or_config("center", config, "align"),
        color=default_or_config("#000000", config, "color"),
        linewidth=default_or_config(None, config, "linewidth"),
        edgecolor="#000000",
        width=default_or_config(0.8, config, "width"))

    hatches = default_or_config([], config, "hatches")
    if hatches:
        group_size = int(len(bars) / len(hatches))
        patterns = []
        for hatch in hatches:
            for i in range(0, group_size):
                patterns.append(hatch)
        for bar, pattern in zip(bars, patterns):
            bar.set_hatch(pattern)


def plot_grouped_bar(config, x_ax_data, y_ax_data):
    # Plot the bars normally
    count_x_ax_data = [i for i in range(1, len(x_ax_data) + 1)]
    plot_bar(config, count_x_ax_data, y_ax_data)

    # Create bars to overlap the vertical lines of the original bars
    new_x_ax_data = [i + 0.5 for i in range(1, len(x_ax_data))]
    new_y_ax_data = [min(y_ax_data[i], y_ax_data[i + 1]) for i in range(0,
        len(y_ax_data) - 1)]

    cur_group = x_ax_data[0]
    num_deleted = 0

    # Remove the bars overlapping a vertical line on the border between groups
    for i in range(1, len(x_ax_data)):
        if x_ax_data[i] != cur_group:
            num_deleted += 1
            del new_x_ax_data[i - num_deleted]
            del new_y_ax_data[i - num_deleted]
            cur_group = x_ax_data[i]

    # Plot the overlapping bars with no linewidth in order to remove the
    # vertical lines
    plt.bar(new_x_ax_data, new_y_ax_data,
        align=default_or_config("center", config, "align"),
        color=default_or_config("#000000", config, "color"),
        linewidth=0,
        width=default_or_config(0.8, config, "width"))

    zero_y_ax_data = [0 for i in range(len(y_ax_data))]

    # Plot 0 height lines on every original bar with the original linewidth,
    # because the overlapping bars also overlap some of the lines on the top of
    # bars, which we basically overlap here again.
    plt.bar(count_x_ax_data, zero_y_ax_data, bottom=y_ax_data,
        align=default_or_config("center", config, "align"),
        color=default_or_config("#000000", config, "color"),
        linewidth=default_or_config(None, config, "linewidth"),
        edgecolor="#000000",
        width=default_or_config(0.8, config, "width"))


def plot_stacked_bar(config, x_ax_data, y_ax_data):
    colors = default_or_config([], config, "color")
    bottoms = default_or_config([], config, "bottom")
    labels = default_or_config([], config, "labels")

    for i in range(len(y_ax_data)):
        if bottoms[i] == "None":
            cur_bottom = 0
        else:
            cur_bottom = y_ax_data[bottoms[i]]

        plt.bar(x_ax_data, y_ax_data[i],
            align=default_or_config("center", config, "align"),
            color=colors[i], bottom=cur_bottom, label=labels[i])


def plot_line(config, x_ax_data, y_ax_data):
    plt.plot(x_ax_data, y_ax_data, default_or_config("-", config, "format"),
            color=default_or_config("#000000", config, "color"))


def plot_multiline(config, x_ax_data, y_ax_data):
    formats = default_or_config(["-"], config, "format")
    colors = default_or_config(["#000000"], config, "color")
    labels = default_or_config([], config, "labels")

    for j in range(len(labels)):
        plt.plot(x_ax_data[j], y_ax_data[j], formats[min(j, len(formats) - 1)],
            color=colors[min(j, len(colors) - 1)], label=labels[j])


def plot_crossed_vlines(config, x_ax_data, y_ax_data):
    colors = default_or_config([], config, "color")
    labels = default_or_config([], config, "labels")

    for i in range(len(x_ax_data) - 1, -1, -1):
        plt.plot(x_ax_data[i], y_ax_data[i],
            default_or_config(",", config, "format"), color=colors[i],
            label=labels[i],
            markersize=default_or_config(",", config, "markersize"),
            markeredgewidth=default_or_config(",", config, "markeredgewidth"))

    # Remove the duplicates in the x ax data for the vlines
    x_ax_data = gather_data(data, default_or_config({}, config, "xdata"))
    x_ax_data = list(set(x_ax_data))
    x_ax_data.sort()

    ax.vlines(x_ax_data, 0, y_ax_data[-1])


def calc_log_2_offset(basenum, offset):
    return 2 ** (log(basenum, 2) + offset)


def log_2_formatter(x, pos):
    if x < 1:
        return "1/" + str(int(1/x))
    else:
        return str(int(x))


def plot_roofline(config, x_ax_data, y_ax_data):
    peak_gflops = default_or_config(0.0, config, "peak_gflops")
    peak_gb_s = default_or_config([], config, "peak_gb_s")
    colors = default_or_config(["#000000"], config, "color")
    labels = default_or_config([], config, "labels")

    xlim = default_or_config([], config, "xlim")
    if xlim:
        min_x = xlim[0]
        max_x = xlim[1]
    else:
        min_x = 2**-20
        max_x = 2**20

    ylim = default_or_config([], config, "ylim")
    if ylim:
        min_y = ylim[0]
        max_y = ylim[1]
    else:
        min_y = 2**-20
        max_y = 2**20

    for i in range(len(x_ax_data)):
        if x_ax_data[i] >= peak_gb_s[-1] * peak_gflops:
            top = peak_gflops
        else:
            top = x_ax_data[i] * peak_gb_s[-1]

        index = i + len(peak_gb_s)

        # Due to the log scale, where 0 is not possible, we use
        # 0.0000000000000000000000000001 instead
        plt.plot([x_ax_data[i], x_ax_data[i]],
            [0.0000000000000000000000000001, top], '--',
            color=colors[min(index, len(colors) - 1)],
            linewidth=5)

        plt.text(calc_log_2_offset(x_ax_data[i], labels[index][1]),
            calc_log_2_offset(min_y, labels[index][2]), labels[index][0],
            rotation=labels[index][3], rotation_mode="anchor")

        plt.plot([x_ax_data[i]], [y_ax_data[i]], 'x', color="#ed2d2e",
            markeredgewidth=3, markersize=15)

    for i in range(len(peak_gb_s)):
        intersection_point = peak_gflops / peak_gb_s[i]
        min_point = min_x * peak_gb_s[i]
        plt.loglog([min_x, intersection_point, max_x],
            [min_point, peak_gflops, peak_gflops], '-',
            color=colors[min(i, len(colors) - 1)], linewidth=5,
            basex=2, basey=2)

        plt.text(calc_log_2_offset(min_x, labels[i][1]),
            calc_log_2_offset(min_point, labels[i][2]), labels[i][0],
            rotation=labels[i][3], rotation_mode="anchor")

    plt.text(calc_log_2_offset(max_x, labels[-1][1]),
        calc_log_2_offset(peak_gflops, labels[-1][2]), labels[-1][0],
        rotation=labels[-1][3])

    formatter = FuncFormatter(log_2_formatter)
    ax.xaxis.set_major_formatter(formatter)
    ax.yaxis.set_major_formatter(formatter)


def plot_grid(config, dimension_data, amount_data):
    ax = plt.gca()
    for i in range(len(dimension_data)):
        start_x = 0
        start_y = 0
        for j in range(amount_data[i]):
            rect = patches.Rectangle((start_x, start_y), dimension_data[i][0],
                dimension_data[i][1],
                facecolor=default_or_config("#000000", config, "color"),
                linewidth=0.5,
                edgecolor="#000000")
            ax.add_patch(rect)
            start_x += dimension_data[i][0]

    ax.set_aspect('equal', 'box')


def plot_violin(config, x_ax_data, y_ax_data):
    min_vals = []
    max_vals = []
    medians = []
    quartile1_ends = []
    quartile3_ends = []

    for current_range in y_ax_data:
        current_range.sort()
        min_vals.append(current_range[0])
        max_vals.append(current_range[-1])
        quartile1_ends.append(np.percentile(current_range, 25))
        medians.append(np.percentile(current_range, 50))
        quartile3_ends.append(np.percentile(current_range, 75))

    parts = plt.violinplot(y_ax_data, showextrema=False)
    color = default_or_config("#000000", config, "color")

    for pc in parts['bodies']:
        pc.set_facecolor(color)
        pc.set_edgecolor('#000000')
        pc.set_alpha(1)

    positions = [i for i in range(1, len(y_ax_data) + 1)]

    plt.scatter(positions, medians, marker='o', color='white', s=30, zorder=3)
    plt.vlines(positions, quartile1_ends, quartile3_ends, lw=5)
    plt.vlines(positions, min_vals, max_vals, lw=1)


def plot_scatter(config, x_ax_data, y_ax_data):
    plt.scatter(x_ax_data, y_ax_data, marker='o',
        c=default_or_config("#000000", config, "color"),
        s=default_or_config(1, config, "point_width"))


if __name__ == "__main__":
    if (len(commandline_params) < 2):
        print("Usage: python3 plot_results.py <result_file_path>")
        exit()

    data_and_plot_funcs = {
        "bar": (gather_data_default, plot_bar),
        "grouped_bar": (gather_data_grouped_bar, plot_grouped_bar),
        "stacked_bar": (gather_data_stacked_bar, plot_stacked_bar),
        "line": (gather_data_default, plot_line),
        "multiline": (gather_data_multiline, plot_multiline),
        "crossed_vlines": (gather_data_crossed_vlines, plot_crossed_vlines),
        "roofline": (gather_data_default, plot_roofline),
        "grid": (gather_data_grid, plot_grid),
        "violin": (gather_data_violin, plot_violin),
        "scatter": (gather_data_default, plot_scatter)
    }

    with open(commandline_params[1], 'r') as json_file:
        config = load(json_file)

    fig = plt.figure()
    # fig = plt.figure(figsize=(5.90666,3.32249625*float(default_or_config(1, config, "height_multiplier"))))

    apply_global_settings(config)

    # Get the csv data from the json file
    data = []
    csvdata = default_or_config([], config, "csv")

    # The data is in a separate file, first get it from there
    if not isinstance(csvdata, list):
        csvfile = "/".join(commandline_params[1].split("/")[:-1]) + "/" + csvdata
        with open(csvfile.strip("/"), 'r') as json_file:
            csvdata = default_or_config([], load(json_file), "csv")

    for row in csvdata:
        data.append(row.split(","))

    # Sort the results if this option is used
    sort_results = default_or_config({}, config, "sort_results")
    if sort_results:
        sort_data(data, sort_results)

    # Filter the results if this option is used
    data_filters = default_or_config({}, config, "data_filters")
    if data_filters:
        data = filter_data(data, data_filters)

    plot_type = default_or_config("", config, "type")

    if plot_type in data_and_plot_funcs:
        gather_data_func, plot_data_func = data_and_plot_funcs[plot_type]

        subplots = default_or_config({}, config, "subplots")
        if subplots:
            # All subplots items should contain one item per subplot, so looping
            # over them should result in an iteration per subplot.
            for i in range(len(next(iter(subplots.values())))):
                for key in subplots:
                    config[key] = subplots[key][i]

                x_ax_data, y_ax_data = gather_data_func(data, config)

                ax = plt.subplot(default_or_config(111, config, "position"))
                plot_data_func(config, x_ax_data, y_ax_data)

                apply_plot_settings(ax, data, x_ax_data, config)

            apply_global_plot_settings(fig, config)
        else:
            ax = fig.add_subplot(111)
            x_ax_data, y_ax_data = gather_data_func(data, config)
            plot_data_func(config, x_ax_data, y_ax_data)
            apply_plot_settings(ax, data, x_ax_data, config)
    else:
        print("Unsupported plot type: " + plot_type)
        exit()

    if len(commandline_params) <= 2:
        plt.show()
    else:
        fig.savefig(commandline_params[2], transparent=True, bbox_inches='tight', pad_inches=0)
