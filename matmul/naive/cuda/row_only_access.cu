#define WIDTH 4096

__global__ void row_only_access(float *C, float *A, float *B) {
    int x = blockIdx.x * block_size_x + threadIdx.x;
    int y = blockIdx.y * block_size_y + threadIdx.y;

    if (x >= WIDTH || y >= WIDTH) {
        return;
    }

    float sum = 0.0;

    for (int k = 0; k < WIDTH; k++) {
        sum += A[y * WIDTH + k] * B[y * WIDTH + k];
    }

    C[y * WIDTH + x] = sum;
}
