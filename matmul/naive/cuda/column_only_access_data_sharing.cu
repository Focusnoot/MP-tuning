#define WIDTH 4096

__global__ void column_only_access_data_sharing(float *C, float *A, float *B) {
    int x = blockIdx.x * block_size_x + threadIdx.x + threadIdx.y % stride;
    int y = blockIdx.y * block_size_y + threadIdx.y;
    float sum = 0.0;

    for (int k=0; k<WIDTH; k++) {
        sum += A[k*WIDTH+x] * B[k*WIDTH+x];
    }

    C[y*WIDTH+x] = sum;
}
