#define WIDTH 4096

__device__ int block_schedule_num = 0;

__device__ void note_block_scheduling(int *D) {
    int this_block_schedule_num = atomicAdd(&block_schedule_num, 1);

    D[blockIdx.x + blockIdx.y * gridDim.x] = this_block_schedule_num;
}

__global__ void block_scheduling(int *D, float *C, float *A, float *B) {
    if (threadIdx.x == 0 && threadIdx.y == 0)
        note_block_scheduling(D);

    int x = blockIdx.x * block_size_x + threadIdx.x;
    int y = blockIdx.y * block_size_y + threadIdx.y;
    float sum = 0.0;

    for (int k=0; k<WIDTH; k++) {
        sum += A[y*WIDTH+k] * B[k*WIDTH+x];
    }

    C[y*WIDTH+x] = sum;
}

