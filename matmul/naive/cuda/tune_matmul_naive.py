#!/usr/bin/env python
import numpy
from sys import argv as commandline_params
from kernel_tuner import tune_kernel, run_kernel
from collections import OrderedDict

def get_matrix_size(kernel_name):
    if kernel_name == "block_size" or kernel_name == "column_only_access" or kernel_name == "row_only_access" or kernel_name == "block_scheduling" or kernel_name == "same_input_matrix_row_only_access" or kernel_name == "same_input_matrix_column_only_access":
        return (4096, 4096)
    # More columns so that the rows are longer so that even with max stride (128) it still fits in the matrix
    elif kernel_name == "row_only_access_strided":
        return (524288, 2048)
    # More columns so that even in the worst case where none of the 1024 threads share data, the matrix can still be indexed +1024
    elif kernel_name == "column_only_access_data_sharing":
        return (5120, 4096)

def get_tune_args(kernel_name):
    A = numpy.random.randn(*get_matrix_size(kernel_name)).astype(numpy.float32)
    B = numpy.random.randn(*get_matrix_size(kernel_name)).astype(numpy.float32)
    C = numpy.zeros((4096,4096),dtype=numpy.float32)

    if kernel_name == "same_input_matrix_row_only_access" or kernel_name == "same_input_matrix_column_only_access":
        args = [C, A]
    else:
        args = [C, A, B]

    if (kernel_name == "block_scheduling"):
        total_blocks = int(4096 * 4096 / (64 * 16))
        D = numpy.zeros(total_blocks, dtype=numpy.int32)
        args.insert(0, D)

    return args

def get_tune_params(kernel_name):
    tune_params = OrderedDict()
    if kernel_name == "block_size" or kernel_name == "column_only_access" or kernel_name == "row_only_access" or kernel_name == "same_input_matrix_row_only_access" or kernel_name == "same_input_matrix_column_only_access":
        tune_params["block_size_x"] = [i for i in range(1,1025)]
        tune_params["block_size_y"] = [i for i in range(1,1025)]
    elif kernel_name == "row_only_access_strided":
        tune_params["block_size_x"] = [16]
        tune_params["block_size_y"] = [64]
        tune_params["stride"] = [1, 2, 4, 8, 16, 32, 64, 128]
    elif kernel_name == "column_only_access_data_sharing":
        tune_params["block_size_x"] = [1]
        tune_params["block_size_y"] = [1024]
        tune_params["stride"] = [1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024]
    elif kernel_name == "block_scheduling":
        tune_params["block_size_x"] = 16
        tune_params["block_size_y"] = 64

    return tune_params

def get_tune_restrictions(kernel_name):
    restrictions = []
    if kernel_name == "block_size" or kernel_name == "column_only_access" or kernel_name == "row_only_access" or kernel_name == "same_input_matrix_row_only_access" or kernel_name == "same_input_matrix_column_only_access":
        restrictions = ["block_size_x*block_size_y>=32", "block_size_x*block_size_y%32==0"]

    return restrictions

def matrix_row_multiplication(matrix1, matrix2):
    result = matrix1.copy()
    for i in range(len(result)):
        temp = 0.0
        for j in range(len(result[0])):
            temp += matrix1[i][j] * matrix2[i][j]
        result[i].fill(temp)
    return result

def matrix_column_multiplication(matrix1, matrix2):
    result = matrix1.copy()
    for i in range(len(result)):
        temp = 0.0
        for j in range(len(result[0])):
            temp += matrix1[j][i] * matrix2[j][i]
        result[i].fill(temp)
    return numpy.transpose(result)

def get_tune_answer(kernel_name, tune_args):
    if kernel_name == "row_only_access":
        return [matrix_row_multiplication(tune_args[1], tune_args[2]), None, None]
    if kernel_name == "column_only_access":
        return [matrix_column_multiplication(tune_args[1], tune_args[2]), None, None]
    elif kernel_name == "same_input_matrix_row_only_access":
        return [matrix_row_multiplication(tune_args[1], tune_args[1]), None]
    elif kernel_name == "same_input_matrix_column_only_access":
        return [matrix_column_multiplication(tune_args[1], tune_args[1]), None, None]

    return [numpy.matmul(tune_args[1], tune_args[2]), None, None]

def get_tune_kernel(kernel_name):
    if kernel_name == "block_scheduling":
        return False

    return True

def tune(kernel_name):
    problem_size = (4096, 4096)

    args = get_tune_args(kernel_name)

    tune_params = get_tune_params(kernel_name)

    perform_tuning = get_tune_kernel(kernel_name)

    restrict = get_tune_restrictions(kernel_name)

    tune_answer = get_tune_answer(kernel_name, args)

    if perform_tuning:
        results = tune_kernel(kernel_name, kernel_name + ".cu", problem_size, args, tune_params, compiler_options=["-lineinfo"], iterations=3, restrictions=restrict, answer=tune_answer, atol=0.001)
    else:
        results = run_kernel(kernel_name, kernel_name + ".cu", problem_size, args, tune_params, compiler_options=["-lineinfo"])
        for i, result in enumerate(results[0]):
            print(str(i) + "," + str(result))

if __name__ == "__main__":
    kernel_name = "block_size"

    # Allow this script to be used for variations of the matmul naive kernel
    if len(commandline_params) > 1:
        kernel_name = commandline_params[1]

    tune(kernel_name)
