#include <stdio.h>
#include <stdlib.h>

#define block_size_z 1
#define block_size_y 52
#define block_size_x 16
#define grid_size_z 1
#define grid_size_y 79
#define grid_size_x 256
#define WIDTH 4096

__global__ void same_input_matrix_row_only_access_16_52(float *B, float *A) {
    int x = blockIdx.x * block_size_x + threadIdx.x;
    int y = blockIdx.y * block_size_y + threadIdx.y;

    if (x >= WIDTH || y >= WIDTH) {
        return;
    }

    float sum = 0.0;

    for (int k = 0; k < WIDTH; k++) {
        sum += A[y * WIDTH + k] * A[y * WIDTH + k];
    }

    B[y * WIDTH + x] = sum;
}

int main() {
    /* h = host, d = device */
    float *hA, *hB, *dA, *dB, ms;
    int size = WIDTH * WIDTH;
    cudaEvent_t start, stop;

    cudaEventCreate(&start);
    cudaEventCreate(&stop);

    hA = (float *)malloc(size * sizeof(float));
    hB = (float *)malloc(size * sizeof(float));

    for (int i = 0; i < size; i++) {
        hA[i] = (float)rand() / (float)RAND_MAX;
        hB[i] = 0.0;
    }

    cudaMalloc(&dA, size * sizeof(float));
    cudaMalloc(&dB, size * sizeof(float));

    cudaMemcpy(dA, hA, size * sizeof(float), cudaMemcpyHostToDevice);

    dim3 block_dim(block_size_x, block_size_y, block_size_z);
    dim3 grid_dim(grid_size_x, grid_size_y, grid_size_z);

    cudaEventRecord(start);
    same_input_matrix_row_only_access_16_52<<<grid_dim, block_dim>>>(dB, dA);
    cudaEventRecord(stop);

    cudaMemcpy(hB, dB, size * sizeof(float), cudaMemcpyDeviceToHost);

    cudaEventSynchronize(stop);
    cudaEventElapsedTime(&ms, start, stop);

    printf("\nTime taken: %.6f milliseconds.\n", ms);

    cudaFree(dA);
    cudaFree(dB);

    free(hA);
    free(hB);

    return 0;
}
