if __name__ == "__main__":
    values = [[i for i in range(6,193)], [i for i in range(3,129)], [i for i in range(8,471)], [i for i in range(3,40)], [i for i in range(2,40)]]

    num_configs = 0

    for block_size_x in values[0]:
        for block_size_y in values[1]:
            if block_size_x*block_size_y >= 32 and block_size_x*block_size_y%32==0:
                for shmem_tile_size in values[2]:
                    if shmem_tile_size%block_size_x==0 or shmem_tile_size%block_size_y==0:
                        for x_elems_per_thread in values[3]:
                            if (shmem_tile_size*block_size_x*x_elems_per_thread + shmem_tile_size*block_size_y*1)*4/1024<=48:
                                for y_elems_per_thread in values[4]:
                                    if (shmem_tile_size*block_size_x*x_elems_per_thread + shmem_tile_size*block_size_y*y_elems_per_thread)*4/1024<=48:
                                        num_configs += 1
                                    else:
                                        break
                            else:
                                break

    print(num_configs)
