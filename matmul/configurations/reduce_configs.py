from csv import reader as csvreader
from sys import argv


def reduce_min(configs, index, value):
    new_configs = []

    for config in configs:
        if config[index] >= value:
            new_configs.append(config)

    return new_configs


def reduce_max(configs, index, value):
    new_configs = []

    for config in configs:
        if config[index] <= value:
            new_configs.append(config)

    return new_configs


if __name__ == "__main__":
    if len(argv) < 2:
        print("Usage: python reduce_configs.py <configs> [<index> <mode> <value>,...]")
        print("Supported modes: min max")
        print("Example: python reduce_configs.py configs.csv 0 min 5 0 max 250")
        exit()

    all_configs = []

    with open(argv[1], 'r') as csvfile:
        reader = csvreader(csvfile)

        for row in reader:
            config = []

            for param in row:
                config.append(int(param))

            all_configs.append(config)

    for i in range(2, len(argv), 3):
        index = int(argv[i])
        mode = argv[i + 1]
        value = int(argv[i + 2])

        if mode == "min":
            all_configs = reduce_min(all_configs, index, value)
        elif mode == "max":
            all_configs = reduce_max(all_configs, index, value)

    for config in all_configs:
        print(",".join(map(str, config)))

    print("Number of configurations printed above: " + str(len(all_configs)))

