from csv import reader as csvreader
from sys import argv


BYTES_PER_ELEM = 4
MAX_SHMEM_BYTES_PER_BLOCK = 49152
ELEMENTS_DIMENSIONS = 4096


if __name__ == "__main__":
    if len(argv) < 2:
        print("Usage: python calculate_shmem_configs.py <naive_configs>")
        exit()

    with open(argv[1], 'r') as csvfile:
        reader = csvreader(csvfile)

        block_size_x_y = [[int(row[0]), int(row[1])] for row in reader]

    shmem_valid_configs = []

    for block_size_x, block_size_y in block_size_x_y:
        for shmem in range(1, ELEMENTS_DIMENSIONS + 1):
            if shmem % block_size_x == 0 or shmem % block_size_y == 0:
                if (block_size_x * shmem + block_size_y * shmem) * BYTES_PER_ELEM <= MAX_SHMEM_BYTES_PER_BLOCK:
                    shmem_valid_configs.append([block_size_x, block_size_y, shmem])

    for row in shmem_valid_configs:
        print(",".join(map(str, row)))

    print("Number of configurations printed above: " + str(len(shmem_valid_configs)))

