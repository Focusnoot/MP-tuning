#define WIDTH 4096

__global__ void shmem_tiling(float *C, float *A, float *B) {
    __shared__ float sA[block_size_y][shmem_tile_size];
    __shared__ float sB[shmem_tile_size][block_size_x];

    int tx = threadIdx.x;
    int ty = threadIdx.y;
    int x = blockIdx.x * block_size_x + tx;
    int y = blockIdx.y * block_size_y + ty;

    float sum = 0.0;
    int k, l, cur_shmem_tile_size;

    cur_shmem_tile_size = shmem_tile_size;

    for (k = 0; k < WIDTH; k += shmem_tile_size) {
        if (k + shmem_tile_size > WIDTH) {
            cur_shmem_tile_size = WIDTH - k;
        }

        __syncthreads();
        for (l = 0; l < cur_shmem_tile_size; l += block_size_x) {
            if (l + tx < cur_shmem_tile_size) {
                sA[ty][l + tx] = A[y * WIDTH + k + l + tx];
            }
        }

        for (l = 0; l < cur_shmem_tile_size; l += block_size_y) {
            if (l + ty < cur_shmem_tile_size) {
                sB[l + ty][tx] = B[(k + l + ty) * WIDTH + x];
            }
        }
        __syncthreads();

        for (l = 0; l < cur_shmem_tile_size; l++) {
            sum += sA[ty][l] * sB[l][tx];
        }
    }

    if (x < WIDTH && y < WIDTH) {
        C[y * WIDTH + x] = sum;
    }
}
