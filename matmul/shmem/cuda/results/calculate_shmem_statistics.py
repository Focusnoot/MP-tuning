from sys import argv
import json
import csv
import math


MATRIX_DIMENSION = 4096
BYTES_PER_ELEM = 4
MAX_THREAD_BLOCKS_PER_SM = 32
MAX_THREADS_PER_SM = 2048
MAX_SHMEM_PER_SM_BYTES = 98304
BARRIERS_PER_ITERATION = 2


def get_total_thread_blocks(block_size_x, block_size_y):
    return math.ceil(MATRIX_DIMENSION / block_size_x) * math.ceil(MATRIX_DIMENSION / block_size_y)


def get_shmem_per_block(block_size_x, block_size_y, shmem_tile_size):
    return (block_size_x + block_size_y) * shmem_tile_size * BYTES_PER_ELEM


# Calculate the number of blocks per SM, taking into account the hardware occupancy and shared memory limits
def get_blocks_per_sm(block_size_x, block_size_y, shmem_tile_size):
    num_blocks_normal = min(MAX_THREAD_BLOCKS_PER_SM, int(MAX_THREADS_PER_SM / (block_size_x * block_size_y)))
    shmem_per_block = get_shmem_per_block(block_size_x, block_size_y, shmem_tile_size)

    return min(int(MAX_SHMEM_PER_SM_BYTES / shmem_per_block), num_blocks_normal)


def get_shmem_per_sm(block_size_x, block_size_y, shmem_tile_size):
    return get_shmem_per_block(block_size_x, block_size_y, shmem_tile_size) * get_blocks_per_sm(block_size_x, block_size_y, shmem_tile_size)


def get_barriers_per_block(shmem_tile_size):
    return math.ceil(MATRIX_DIMENSION / shmem_tile_size) * BARRIERS_PER_ITERATION


def get_total_barriers(block_size_x, block_size_y, shmem_tile_size):
    return get_total_thread_blocks(block_size_x, block_size_y) * get_barriers_per_block(shmem_tile_size)


def get_elements_loaded_between_barriers(block_size_x, block_size_y, shmem_tile_size):
    return block_size_x * shmem_tile_size + block_size_y * shmem_tile_size


def get_total_elements_loaded(block_size_x, block_size_y, shmem_tile_size):
    return get_elements_loaded_between_barriers(block_size_x, block_size_y, shmem_tile_size) * get_total_barriers(block_size_x, block_size_y, shmem_tile_size)


def get_frac_elements_loaded_between_barriers(block_size_x, block_size_y, shmem_tile_size):
    return get_elements_loaded_between_barriers(block_size_x, block_size_y, shmem_tile_size) / get_total_elements_loaded(block_size_x, block_size_y, shmem_tile_size)


def get_threads_per_sm(block_size_x, block_size_y, shmem_tile_size):
    return block_size_x * block_size_y * get_blocks_per_sm(block_size_x, block_size_y, shmem_tile_size)


if __name__ == "__main__":
    if len(argv) < 3:
        print("Usage: python calculate_shmem_usage_per_config.py <result_file> <allowed_shmem_configs_file> <statistics>")
        print("Expected format of result_file csv: 'block_size_x,block_size_y,shmem_tile_size,time'")
        print("Expected format of allowed_shmem_configs_file csv: 'block_size_x,block_size_y,shmem_tile_size'")
        print("Supported statistics: shmem_per_sm blocks_per_sm total_barriers shmem_per_block frac_elems_loaded_barriers threads_per_sm")
        exit()

    with open(argv[1], 'r') as f:
        data = json.load(f)

    csv_results = [row.split(',') for row in data['csv']]

    with open(argv[2], 'r') as f:
        reader = csv.reader(f)
        configs = [row for row in reader]

    statistics_to_print = argv[3:]

    for row in csv_results:
        if row[:3] in configs:
            final_str = "\"" + ",".join(row)

            if "shmem_per_sm" in statistics_to_print:
                final_str += "," + str(get_shmem_per_sm(int(row[0]), int(row[1]), int(row[2])))
            if "blocks_per_sm" in statistics_to_print:
                final_str += "," + str(get_blocks_per_sm(int(row[0]), int(row[1]), int(row[2])))
            if "total_barriers" in statistics_to_print:
                final_str += "," + str(get_total_barriers(int(row[0]), int(row[1]), int(row[2])))
            if "shmem_per_block" in statistics_to_print:
                final_str += "," + str(get_shmem_per_block(int(row[0]), int(row[1]), int(row[2])))
            if "frac_elems_loaded_barriers" in statistics_to_print:
                final_str += "," + str(get_frac_elements_loaded_between_barriers(int(row[0]), int(row[1]), int(row[2])))
            if "threads_per_sm" in statistics_to_print:
                final_str += "," + str(get_threads_per_sm(int(row[0]), int(row[1]), int(row[2])))

            final_str += "\","

            print(final_str)
