from math import gcd
from sys import argv


def lcm(num1, num2):
    return int(num1 * num2 / gcd(num1, num2))


def multi_lcm(numbers):
    cur_lcm = lcm(numbers[0], numbers[1])

    for i in range(2, len(numbers)):
        cur_lcm = lcm(numbers[i], cur_lcm)

    return cur_lcm


if __name__ == "__main__":
    if len(argv) < 4:
        print("Usage: python3.6+ least_common_multiple.py <start> <end> <step>")
        print("Example: \"python3.6 least_common_multiple.py 2 6 2\" prints " +
            "the least common multiple of [2, 4, 6].")
        exit()

    num_blocks = list(range(int(argv[1]),int(argv[2]) + 1,int(argv[3])))
    print(num_blocks)
    block_sizes = [32, 64, 128, 256, 512, 1024]

    numbers = [blocks * block_size for block_size in block_sizes for blocks in num_blocks]
    numbers = list(set(numbers))
    numbers.sort()
    print(multi_lcm(numbers))
