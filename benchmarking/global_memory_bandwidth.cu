#define WARP_SIZE 32
#define TOTAL_THREADS grid_div_x * block_size_x

#ifndef vector
#define vector 1
#endif

#if (vector == 1)
#define FLOAT_VECTOR float
#define END array_size
#elif (vector == 2)
#define FLOAT_VECTOR float2
#define END array_size / vector
#elif (vector == 4)
#define FLOAT_VECTOR float4
#define END array_size / vector
#endif

__global__ void global_memory_bandwidth(FLOAT_VECTOR *A, float *B) {
    int global_thread_id = blockIdx.x * block_size_x + threadIdx.x;
    float sum;

    #pragma unroll
    for (int i = 0; i < END; i += TOTAL_THREADS) {
        FLOAT_VECTOR v = A[i + global_thread_id];
        #if (vector==1)
        sum += v;
        #elif (vector == 2)
        sum += v.x + v.y;
        #elif (vector == 4)
        sum += v.x + v.y + v.z + v.w;
        #endif
    }

    B[global_thread_id] = sum;
}
