#!/usr/bin/env python
import numpy
from math import gcd, floor
from sys import argv
from kernel_tuner import tune_kernel
from collections import OrderedDict


def lcm(num1, num2):
    return int(num1 * num2 / gcd(num1, num2))


def multi_lcm(numbers):
    cur_lcm = lcm(numbers[0], numbers[1])

    for i in range(2, len(numbers)):
        cur_lcm = lcm(numbers[i], cur_lcm)

    return cur_lcm


def get_tune_args(kernel_name, array_a_size):
    A = numpy.arange(1, array_a_size + 1).astype(numpy.float32)
    # The max size B needs is 1024 * 48 (the largest config) = 49152
    B = numpy.zeros(49152).astype(numpy.float32)

    return [A, B]


def get_tune_params(kernel_name):
    tune_params = OrderedDict()
    if kernel_name == "global_memory_bandwidth" or kernel_name == "l1_bandwidth" or kernel_name == "l2_bandwidth":
        tune_params["block_size_x"] = [32, 64, 128, 256, 512, 1024]
        tune_params["vector"] = [1, 2, 4]

    return tune_params


def tune(kernel_name, tb_start, tb_end, tb_step):
    cur = tb_start
    tune_params = get_tune_params(kernel_name)

    # We calculate the least common multiple for each range of numbers until it
    # does not fit in an int anymore, and then continue with the next range
    # until we reached the tb_end.
    while cur <= tb_end:
        old_cur = cur
        array_lcm = 1
        new_array_lcm = 1

        while True:
            array_lcm = new_array_lcm

            cur += tb_step

            numbers = [blocks * block_size for block_size in tune_params["block_size_x"] for blocks in list(range(old_cur,cur,tb_step))]
            numbers = list(set(numbers))
            numbers.sort()

            new_array_lcm = multi_lcm(numbers)

            if new_array_lcm >= 2**31:
                cur -= tb_step
                break
            elif cur > tb_end:
                array_lcm = new_array_lcm
                break


        problem_size = floor(2**31 / array_lcm) * array_lcm

        args = get_tune_args(kernel_name, problem_size)

        # Since kernel tuner doesn't allow us to directly choose the grid size, we
        # still do this using the following tedious approach of multiple tune runs
        # and a special grid_div_x expression to allow us to get our desired grid_div_x
        for cur_grid_div_x in list(range(old_cur, cur, tb_step)):
            tune_params["grid_div_x"] = [cur_grid_div_x]
            tune_params["array_size"] = [problem_size]
            grid_div_x_expression = "block_size_x*" + str(int(problem_size / cur_grid_div_x)) + "/block_size_x"
            results = tune_kernel(kernel_name, kernel_name + ".cu", problem_size, args, tune_params, grid_div_x=[grid_div_x_expression], compiler_options=["-lineinfo"], iterations=3)


if __name__ == "__main__":
    # Allow this script to be used for variations of the matmul naive kernel
    if len(argv) < 5:
        print("Usage: python3.6+ tune_benchmarking.py <kernel_name> <thread_block_start> <thread_block_end> <thread_block_step>")
        exit()

    tune(argv[1], int(argv[2]), int(argv[3]), int(argv[4]))
