import json
from sys import argv as commandline_params
from collections import OrderedDict


def print_results(results, is_l2):
    for key in results:
        final_str = "\"" + key[0] + "," + key[1]
        for result in results[key]:
            final_str += "," + result[1]

        mem_moved = int(key[0]) * int(key[1]) + int(result[0])

        if is_l2:
            mem_moved -= 6144 * int(key[0]) * int(key[1])

        final_str += "," + str(mem_moved * 4)

        print(final_str + "\",")


if __name__ == "__main__":
    if len(commandline_params) < 2:
        print("Usage: python convert_benchmark_results.py <glmem_results_json> optional:<l1_mem_results_json> optional:<is_l2_instead_of_l1>")
        exit()

    with open(commandline_params[1], 'r') as f:
        data = json.load(f)

    glmem_csvdata = [row.split(',') for row in data["csv"]]

    glmem_results = OrderedDict()

    for row in glmem_csvdata:
        if (row[0],row[2]) not in glmem_results:
            glmem_results[(row[0],row[2])] = [(row[3], row[4])]
        else:
            glmem_results[(row[0],row[2])].append((row[3], row[4]))

    if len(commandline_params) == 2:
        print_results(glmem_results, False)
    else:
        with open(commandline_params[2], 'r') as f:
            data = json.load(f)

        cache_csvdata = [row.split(',') for row in data["csv"]]

        cache_results = OrderedDict()
        for i in range(len(cache_csvdata)):
            result = (cache_csvdata[i][3], str(float(cache_csvdata[i][4]) - float(glmem_csvdata[i][4])))
            if (cache_csvdata[i][0],cache_csvdata[i][2]) not in cache_results:
                cache_results[(cache_csvdata[i][0],cache_csvdata[i][2])] = [result]
            else:
                cache_results[(cache_csvdata[i][0],cache_csvdata[i][2])].append(result)

        if len(commandline_params) == 4:
            print_results(cache_results, True)
        else:
            print_results(cache_results, False)
